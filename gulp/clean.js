'use strict';

var del = require('del');

module.exports = function (gulp, args) {
  return del(args.baseFolder);
};
