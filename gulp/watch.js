'use strict';

module.exports = function (gulp, args) {
    gulp.watch(args.srcFolder + '/scss/**/*.scss', ['csslint', 'css']);
    gulp.watch(args.srcFolder + '/js/**/*.js', ['jshint', 'javascript']);
    gulp.watch(args.srcFolder + '/views/**/*.html', ['htmlhint', 'html']);
};
