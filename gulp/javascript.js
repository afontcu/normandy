'use strict';

var
  rename = require('gulp-rename'),
  uglify = require('gulp-uglify'),
  sourcemaps = require('gulp-sourcemaps'),
  browserSync = require('browser-sync');

module.exports = function (gulp, args) {
  return gulp.src(args.srcFolder + '/js/scripts.js')
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(args.devFolder + '/assets/js'))
    .pipe(gulp.dest(args.distFolder + '/assets/js'))
    .pipe(browserSync.reload({ stream: true, once: true }));
};
