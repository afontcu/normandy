# Normandy
Normandy is an extensible Sass-based front end boilerplate to be used as a setup
for web related projects.


Normandy leaves all the UI decisions to the developer/designer using this package.
It just provides an initial structure and tries to focus opinionated decisions on naming conventions and structural decisions.


## Some Features
* Object-Oriented CSS architecture boilerplate built upon [InuitCSS](https://github.com/inuitcss/inuitcss).
* Live-reloading browser with [BrowserSync](https://browsersync.io) and CSS injection.
* Compiled and minified assets on the fly.
* Autoprefixer to ensure [old browser compatibility](https://github.com/postcss/autoprefixer) (up to last 4 versions).
* CSS and JS sourcemapping to ease debugging [using DevTools](https://medium.com/@toolmantim/getting-started-with-css-sourcemaps-and-in-browser-sass-editing-b4daab987fb0).
* Sass linting using [Stylelint](https://stylelint.io/) to mantain code based on [Sass-guidelin.es](https://sass-guidelin.es).
* HTML linting using [HTMLHint](https://github.com/yaniswang/HTMLHint).
* Javascript linting using [JSHint](http://jshint.com/).
* Shame.css to [gather all the hacky CSS](https://csswizardry.com/2013/04/shame-css/) to ged rid of it ASAP.


## Getting Started
Getting started with Normandy is as easy as running

 `npm run first-time`

Please notice that you must have the usual dependencies installed on your 
computer (such as Node.js or Sass).


## Usage
Just run `gulp` to launch a live-reloading project in localhost.

Please read all the information on the Gulpfile file in order to learn about
all the available tasks and how to add new ones.


## Development
The `build/` directory. Contains all the deployable site, including HTML and
compiled assets. This folders has two childs:


The `build/dev/` folder, useful to help developers do their thing using their
tools (i.e., this folder has non-minified HTML), and


The a `build/dist/` folder, ready to deploy with minified assets. This is the 
folder you want to deploy to your production server.


Finally, the `src/` directory is used to keep development files out of
deployment. Edit your JS, Sass, HTML views and other assets there.


These folder names can be changed through Gulpfile (and renaming the folders,
obviously). Make sure you update .gitignore accordingly.


## Testing
While Normandy doesn't provide a testing suite, it still provides a
`npm run test` script to lint HTML, CSS and JS files. The default Gulp tasks
lints all files as well.

This npm script could be extended and improved using another testing tools such
as Mocha or Jasmine. Please all the testing logic to the npm run test task.
