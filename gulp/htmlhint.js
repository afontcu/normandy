'use strict';

var htmlhint = require('gulp-htmlhint');

module.exports = function (gulp, args) {
  return gulp.src(args.srcFolder + '/views/**/*.html')
  .pipe(htmlhint('.htmlhintrc'))
  .pipe(htmlhint.reporter());
};
