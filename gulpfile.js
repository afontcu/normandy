'use strict';

/* ==========================================================================
   NORMANDY GULPFILE
   ========================================================================== */

/**
 * Gulpfile is the task automation tool used to process all the source files
 * and get the build files.
 *
 * This Gulpfile uses Require Task to load all tasks from /gulp directory.
 * The name of the task is inferred from the directory structure,
 * e.g. gulp clean or gulp csslint.
 *
 * Please notice that you must add just one task per file.
 */


/**
 * Here you can customise your source, distribution and development directories
 * If you change them, make sure you update your .gitignore accordingly to keep
 * the baseFolder and its subfolders out of the repository.
 */

var
  srcFolder = 'src',
  baseFolder = 'build',
  distFolder = baseFolder + '/dist',
  devFolder = baseFolder + '/dev';



/**
 * USAGE
 *
 * Just run `gulp` to start the default task, which is defined in this very
 * file.
 *
 * Gulp serves files from devFolder by default using BrowserSync.
 * Add --production flag to gulp command in order to serve files from
 * distFolder.
 */

var
  gulp = require('gulp'),
  gulpRequireTasks = require('gulp-require-tasks'),
  browserSync = require('browser-sync');


// Load all the tasks from /gulp directory.
gulpRequireTasks({
  path: process.cwd() + '/gulp',
  arguments: [{srcFolder, distFolder, devFolder}]
});



/**
 * BrowserSync on /distFolder or /devFolder depending on --production flag.
 *
 * Please check https://browsersync.io/ for further BrowserSync features.
 * Gulpfile will automatically launch a new desktop tab for your project.
 * Make sure you check your terminal output to access the BrowserSync
 * control panel.
 */

gulp.task('browser-sync', function () {
  var productionFlag = process.argv.indexOf('--production') > -1;

  browserSync.init(null, {server: {
      baseDir: productionFlag ? distFolder : devFolder
  }});
});


/**
 * The default task is the only task defined directly on gulpfile.js.
 *
 * Just run `gulp` or `gulp --production` to launch all the tasks and start a
 * watching task to trigger specific tasks while developing.
 *
 * Everytime you change a CSS, JS or HTML file, Gulpfile will lint the files
 * and process them, adding them up to the destination folders and doing its
 * magic.
 *
 * Make sure to add your custom tasks if you want them to be included in the
 * watching loop.
 */

gulp.task('default',
  [
    'csslint', 'css',
    'jshint', 'javascript',
    'htmlhint', 'html',
    'browser-sync',
    'watch'
  ]);
