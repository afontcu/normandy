'use strict';

var stylelint = require('gulp-stylelint');

module.exports = function (gulp, args) {
  return gulp
    .src(args.srcFolder + '/scss/**/*.scss')
    .pipe(stylelint({
      reporters: [
        { formatter: 'string', console: true }
      ]
    }));
};
