'use strict';

var
  rename = require('gulp-rename'),
  sass = require('gulp-sass'),
  cssnano = require('gulp-cssnano'),
  autoprefixer = require('gulp-autoprefixer'),
  sourcemaps = require('gulp-sourcemaps'),
  browserSync = require('browser-sync');

module.exports = function (gulp, args) {
  return gulp.src(args.srcFolder + '/scss/main.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer('last 4 version'))
    .pipe(cssnano())
    .pipe(rename({ suffix: '.min' }))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(args.devFolder + '/assets/css'))
    .pipe(gulp.dest(args.distFolder + '/assets/css'))
    .pipe(browserSync.reload({ stream: true }));
};
