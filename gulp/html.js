'use strict';

var
  htmlmin = require('gulp-htmlmin'),
  browserSync = require('browser-sync');

module.exports = function (gulp, args) {
  return gulp.src(args.srcFolder + '/views/**/*.html')
    .pipe(gulp.dest(args.devFolder))
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest(args.distFolder))
    .pipe(browserSync.reload({ stream: true, once: true }));
};
