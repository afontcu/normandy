'use strict';

var jshint = require('gulp-jshint');

module.exports = function (gulp, args) {
  gulp.src(args.srcFolder + '/js/scripts.js')
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'));
};
